#!/usr/bin/env python3

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_hold(self):
        '''Army holds'''
        self.assertEqual(diplomacy_read("A Madrid Hold\n"),
                         ['A', "Madrid", None])

    def test_read_move(self):
        '''Army moves'''
        self.assertEqual(diplomacy_read("B Barcelona Move Madrid\n"),
                         ['B', "Madrid", None])

    def test_read_support(self):
        '''Army attempts to support'''
        self.assertEqual(diplomacy_read("C London Support B\n"),
                         ['C', "London", 'B'])

    def test_read_invalid_action(self):
        '''Army pursues invalid action'''
        with self.assertRaises(AssertionError):
            diplomacy_read("D Nowhere Impossible\n")

    def test_read_invalid_army(self):
        '''Army has an invalid name'''
        with self.assertRaises(AssertionError):
            diplomacy_read("e Austin Hold\n")
        with self.assertRaises(AssertionError):
            diplomacy_read("EE Austin Hold\n")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        '''Single army'''
        scenario = [['A', 'Madrid', None]]
        outcome = [['A', 'Madrid', None]]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_2(self):
        '''Two way battle with a victor'''
        scenario = [['A', 'Madrid', None], [
            'B', 'Madrid', None], ['C', 'London', 'B']]
        outcome = [['A', '[dead]', None], [
            'B', 'Madrid', None], ['C', 'London', 'B']]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_3(self):
        '''Two way battle with equal power'''
        scenario = [['A', 'Madrid', None], ['B', 'Madrid', None]]
        outcome = [['A', '[dead]', None], ['B', '[dead]', None]]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_4(self):
        '''Invalidated support in two way battle'''
        scenario = [['A', 'Madrid', None], ['B', 'Madrid', None],
                    ['C', 'London', 'B'], ['D', 'London', None]]
        outcome = [['A', '[dead]', None], ['B', '[dead]', None],
                   ['C', '[dead]', None], ['D', '[dead]', None]]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_5(self):
        '''Three way battle with equal power'''
        scenario = [['A', 'Madrid', None], [
            'B', 'Madrid', None], ['C', 'Madrid', None]]
        outcome = [['A', '[dead]', None], [
            'B', '[dead]', None], ['C', '[dead]', None]]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_6(self):
        '''Three way battle with a victor'''
        scenario = [['A', 'Madrid', None], ['B', 'Madrid', None],
                    ['C', 'Madrid', None], ['D', 'Paris', 'B']]
        outcome = [['A', '[dead]', None], ['B', 'Madrid', None],
                   ['C', '[dead]', None], ['D', 'Paris', 'B']]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_7(self):
        '''Three way battle with a two way tie'''
        scenario = [['A', 'Madrid', None], ['B', 'Madrid', None], [
            'C', 'Madrid', None], ['D', 'Paris', 'B'], ['E', 'Austin', 'A']]
        outcome = [['A', '[dead]', None], ['B', '[dead]', None], [
            'C', '[dead]', None], ['D', 'Paris', 'B'], ['E', 'Austin', 'A']]

        self.assertEqual(diplomacy_eval(scenario), outcome)

    def test_eval_invalid_scenario(self):
        '''No armies'''
        scenario = []

        with self.assertRaises(AssertionError):
            diplomacy_eval(scenario)

    # -----
    # print
    # -----

    def test_print_1(self):
        '''Army that did not support'''
        w = StringIO()
        diplomacy_print(w, ['A', 'Madrid', None])

        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        '''Army that did support'''
        w = StringIO()
        diplomacy_print(w, ['C', 'London', "B"])

        self.assertEqual(w.getvalue(), "C London\n")

    def test_print_3(self):
        '''Defeated army that did not support'''
        w = StringIO()
        diplomacy_print(w, ['A', '[dead]', None])

        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_4(self):
        '''Defeated army that did support'''
        w = StringIO()
        diplomacy_print(w, ['C', '[dead]', "B"])

        self.assertEqual(w.getvalue(), "C [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        '''Single army'''
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        '''Two way battle with a victor'''
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        '''Two way battle with equal power'''
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        '''Invalidated support in two way battle'''
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        '''Three way battle with equal power'''
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_6(self):
        '''Three way battle with a victor'''
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_7(self):
        '''Three way battle with a two way tie'''
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_8(self):
        '''Inputs not alphabetical (Two way battle with a victor)'''
        r = StringIO(
            "A Madrid Hold\nC London Support B\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)

        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
"""
