from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_eval, diplomacy_read, diplomacy_solve, diplomacy_write, Army, Bracket

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
	# ----
	# read
	# ----

	def test_read1(self):
		#text file should have: 
		#A Madrid Hold
		#B Barcelona Move Madrid
		#C London Support B
		s = "A Madrid Hold"
		return_army = diplomacy_read(s)
		army = Army("A", "Madrid", "Hold")
		self.assertEqual(return_army, army)

	def test_read2(self):
		#text file should have: 
		#A Madrid Hold
		#B Barcelona Move Madrid
		#C London Move Madrid
		#D Paris Support B
		#E Austin Support A
		s = "B Barcelona Move Madrid"
		return_army = diplomacy_read(s)
		army = Army("B", "Barcelona", "Move", "Madrid")
		self.assertEqual(return_army, army)

	def test_read3(self):
		#text file should have: 
		#A Madrid Hold
		#B Barcelona Move Madrid
		#C London Move Madrid
		#D Paris Support B	
		s = "D Paris Support B"
		return_armies = diplomacy_read(s)
		army = Army("D", "Paris", "Support", "B")
		self.assertEqual(return_armies, army)

	# ----
	# eval
	# ----

	def test_eval_1(self):
		array_armies = []
		array_armies += [Army("A", "Madrid", "Hold")]
		array_armies += [Army("B", "Barcelona", "Move", "Madrid")]
		array_armies[1].numSupport += 1
		array_armies += [Army("C", "London", "Support", "B")]
		final_pos = diplomacy_eval(array_armies)
		self.assertEqual(final_pos[0].location, "[dead]")
		self.assertEqual(final_pos[1].location, "Madrid")
		self.assertEqual(final_pos[2].location, "London")

	def test_eval_2(self):
		array_armies = []
		array_armies += [Army("A", "Madrid", "Hold")]
		array_armies[0].numSupport += 1
		array_armies += [Army("B", "Barcelona", "Move", "Madrid")]
		array_armies[1].numSupport += 1
		array_armies += [Army("C", "London", "Move", "Madrid")]
		array_armies += [Army("D", "Paris", "Support", "B")]
		array_armies += [Army("E", "Austin", "Support", "A")]
		final_pos = diplomacy_eval(array_armies)
		self.assertEqual(final_pos[0].location, "[dead]")
		self.assertEqual(final_pos[1].location, "[dead]")
		self.assertEqual(final_pos[2].location, "[dead]")
		self.assertEqual(final_pos[3].location, "Paris")
		self.assertEqual(final_pos[4].location, "Austin")

	def test_eval_3(self):
		array_armies = []
		array_armies += [Army("A", "Madrid", "Hold")]
		array_armies += [Army("B", "Barcelona", "Move", "Madrid")]
		array_armies[1].numSupport += 1
		array_armies += [Army("C", "London", "Move", "Madrid")]
		array_armies += [Army("D", "Paris", "Support", "B")]
		final_pos = diplomacy_eval(array_armies)
		self.assertEqual(final_pos[0].location, "[dead]")
		self.assertEqual(final_pos[1].location, "Madrid")
		self.assertEqual(final_pos[2].location, "[dead]")
		self.assertEqual(final_pos[3].location, "Paris")

	# -----
	# write
	# -----

	def test_print1(self):
		w = StringIO()
		array_armies = []
		array_armies += [Army("A", "Madrid", "Hold")]
		array_armies += [Army("B", "Barcelona", "Move", "Madrid")]
		array_armies += [Army("C", "London", "Support")]
		diplomacy_write(w, array_armies)
		self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London")

	def test_print2(self):
		w = StringIO()
		array_armies = []
		array_armies += [Army("A", "Madrid", "Hold")]
		array_armies += [Army("B", "Barcelona", "Move", "Madrid")]
		array_armies += [Army("C", "London", "Move", "Madrid")]
		array_armies += [Army("D", "Paris", "Support", "B")]
		array_armies += [Army("E", "Austin", "Support", "A")]
		diplomacy_write(w, array_armies)
		self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Paris\nE Austin")

	def test_print3(self):
		w = StringIO()
		array_armies = []
		array_armies += [Army("A", "Madrid", "Hold")]
		array_armies += [Army("B", "Barcelona", "Move", "Madrid")]
		array_armies += [Army("C", "London", "Move", "Madrid")]
		array_armies += [Army("D", "Paris", "Support", "B")]
		diplomacy_write(w, array_armies)
		self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Paris")

	# -----
	# solve
	# -----

	def test_solve1(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC London")

	def test_solve2(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin")

	def test_solve3(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
"""
