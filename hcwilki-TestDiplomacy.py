#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve




class TestDiplomacy (TestCase):
	# ----
	# read
	# ----

	def test_read(self):
		s = "A Madrid Hold\n"
		i, j, k, l = diplomacy_read(s)
		self.assertEqual(i,  "A")
		self.assertEqual(j, "Madrid")
		self.assertEqual(k, "Hold")

	def test_read2(self):
		s = "B Barcelona Move Madrid\n"
		i, j, k, l = diplomacy_read(s)
		self.assertEqual(i,  "B")
		self.assertEqual(j, "Barcelona")
		self.assertEqual(k, "Move")
		self.assertEqual(l, "Madrid")

	def test_read3(self):
		s = "C London Move Madrid\n"
		i, j, k, l = diplomacy_read(s)
		self.assertEqual(i,  "C")
		self.assertEqual(j, "London")
		self.assertEqual(k, "Move")
		self.assertEqual(l, "Madrid")


	def test_read_4(self):
		s = "D Paris Support B\n"
		i, j, k, l = diplomacy_read(s)
		self.assertEqual(i,  "D")
		self.assertEqual(j, "Paris")
		self.assertEqual(k, "Support")
		self.assertEqual(l, "B")



	# # -----
	# # print
	# # -----

	def test_print(self):
		w = StringIO()
		diplomacy_print(w, "A", "Madrid")
		self.assertEqual(w.getvalue(), "A Madrid\n")

	def test_print_2(self):
		w = StringIO()
		diplomacy_print(w, "B", "[dead]")
		self.assertEqual(w.getvalue(), "B [dead]\n")

	def test_print_3(self):
		w = StringIO()
		diplomacy_print(w, "D", "Paris")
		self.assertEqual(w.getvalue(), "D Paris\n")

	def test_print_4(self):
		w = StringIO()
		diplomacy_print(w, "C", "[dead]")
		self.assertEqual(w.getvalue(), "C [dead]\n")


	# # -----
	# # solve
	# # -----

	def test_solve(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")


	def test_solve_2(self):
		r = StringIO("A Minneapolis Hold\nB Bangkok Move Minneapolis\nC Cairo Support B\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Minneapolis\nC Cairo\n")

	def test_solve_3(self):
		r = StringIO("A NewYork Hold\nB Boston Move NewYork\nC Vancouver Move NewYork\nD Perth Support B\nE Dublin Support A\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Perth\nE Dublin\n")

	def test_solve_4(self):
		r = StringIO("A Paris Hold\nB London Support A\nC Madrid Move London\nD Tokyo Move Paris\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
