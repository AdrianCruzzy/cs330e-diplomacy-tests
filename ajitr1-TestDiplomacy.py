#!/usr/bin/env python3

# -------------------------------
# by Rishi Venkat and Ajit Ramamohan
#

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Buda Hold\nB Atlanta Move Buda\nC Brussels Move Buda\nD Seattle Support B\nE Houston "
                     "Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Seattle\nE Houston\n")

    def test_solve_2(self):
        r = StringIO("A Denver Hold\nB Budapest Move Denver\nC Sacramento Support B\nD Cleveland Move Sacramento\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Boston Hold\nB Miami Move Boston\nC Dallas Move Boston\nD Philadelphia Support B\nE Phoenix "
                     "Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Boston\nC [dead]\nD Philadelphia\nE Phoenix\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover

$ cat TestDiplomacy.out

...
----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          59      0     46      0   100%
TestDiplomacy.py      21      0      2      0   100%
--------------------------------------------------------------
TOTAL                 80      0     48      0   100%
"""
