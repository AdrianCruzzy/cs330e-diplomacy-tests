#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        team, city, action, target = diplomacy_read(s)
        self.assertEqual(team, "A")
        self.assertEqual(city, "Madrid")
        self.assertEqual(action, "Hold")
        self.assertEqual(target, None)

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        team, city, action, target = diplomacy_read(s)
        self.assertEqual(team, "B")
        self.assertEqual(city, "Barcelona")
        self.assertEqual(action, "Move")
        self.assertEqual(target, "Madrid")

    def test_read_3(self):
        s = "D Austin Move London\n"
        team, city, action, target = diplomacy_read(s)
        self.assertEqual(team, "D")
        self.assertEqual(city, "Austin")
        self.assertEqual(action, "Move")
        self.assertEqual(target, "London")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold", None],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Support", "B"]])
        self.assertEqual(v, [["A", "[dead]"],["B","Madrid"],["C", "London"]])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", "Hold", None]])
        self.assertEqual(v, [["A", "Madrid"]])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Move", "Barcelona"],["B", "Barcelona", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "Barcelona"],["B","Madrid"]])

    def test_eval_4(self):
        v = diplomacy_eval([["A", "Madrid", "Hold", None],["B", "Barcelona", "Move", "Madrid"],["C", "London", "Move", "Madrid"]])
        self.assertEqual(v, [["A", "[dead]"],["B","[dead]"],["C", "[dead]"]])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w,[["A","Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w,[["A","[dead]"],["B","[dead]"],["C","[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w,[["A","[dead]"],["B","Madrid"],["C","[dead]"],["D","Paris"]])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
unit test
"""