# TestDiplomacy.py
# test cases for Diplomacy.py
# Riley Sample
# rcs3396

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_eval, diplomacy_read, diplomacy_print


class TestDiplomacy(TestCase):


    # -----------------------
    # Testing diplomacy_eval
    # -----------------------

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"]])
        self.assertEqual(v, ["A Madrid"])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"],
                             ["C", "London", "Support", "B"]])
        self.assertEqual(v, ["A [dead]", "B Madrid", "C London"])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"],
                             ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])


    # -----------------------
    # Testing diplomacy_solve
    # -----------------------

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Austin Move London\nC Houston Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Dallas Move Austin\nB NewYork Move Austin\nC Houston Support A\nD London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Houston\nD London\n")


    # -----------------------
    # Testing diplomacy_print
    # -----------------------

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A Madrid"])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C London"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


    # -----------------------
    # Testing diplomacy_read
    # -----------------------

    def test_read_1(self):
        s = "A Madrid Hold\n"
        arr = diplomacy_read(s)
        self.assertEqual(arr, ["A", "Madrid", "Hold"])

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        arr = diplomacy_read(s)
        self.assertEqual(arr, ["B", "Barcelona", "Move", "Madrid"])

    def test_read_3(self):
        s = "C London Support B\n"
        arr = diplomacy_read(s)
        self.assertEqual(arr, ["C", "London", "Support", "B"])


if __name__ == "__main__": #pragma: no cover
    main()