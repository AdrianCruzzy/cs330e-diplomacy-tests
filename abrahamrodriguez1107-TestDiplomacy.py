#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Abraham Rodriguez
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        h, i, j, k = diplomacy_read(s)
        self.assertEqual(h, ['A', 'B', 'C'])
        self.assertEqual(i, ['Madrid', 'Barcelona', 'London'])
        self.assertEqual(j, ['Hold', 'Move', 'Support'])
        self.assertEqual(k, ['', 'Madrid', 'B'])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(['A', 'B', 'C'], ['Madrid', 'Barcelona', 'London'], ['Hold', 'Move', 'Support'], ['', 'Madrid', 'B'])
        self.assertEqual(v, "A [dead]\nB Madrid\nC London\n")

    # -----
    # print
    # -----

    def test_print_1(self):
        v = diplomacy_print("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n", "A [dead]\nB Madrid\nC London\n")
        self.assertEqual(v, "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n\nA [dead]\nB Madrid\nC London\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB Madrid\nC London\n")
        
    def test_solve_2(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_3(self):
        r = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n"
        w = diplomacy_solve(r)
        self.assertEqual(w, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
--------------------------------------------------------------
TOTAL                 44      0      2      0   100%
"""