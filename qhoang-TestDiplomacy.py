
# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import *



class TestDiplomacy (TestCase):
    
    # -----
    # read
    # -----

    def test_read_1(self):
        s = "A Madrid Hold"
        army = diplomacy_read(s)
        self.assertEqual(str(army),"A Madrid 0 supporting armies")
    
    def test_read_2(self):
        s = "B Barcelona Support A"
        army = diplomacy_read(s)
        self.assertEqual(str(army),"B Barcelona 0 supporting armies")
    
    def test_read_3(self):
        s = "B Barcelona Move Austin"
        army = diplomacy_read(s)
        self.assertEqual(str(army),"B Barcelona 0 supporting armies")
    
    # -----
    # solve
    # -----
    def test_solve_0(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC Austin Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")    
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")    
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")  
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")  
    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\n") 
    def test_solve_9(self):
        r = StringIO("A Madrid Move London\nB Barcelona Support A\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Barcelona\nC [dead]\n") 
    def test_solve_10(self):
        r = StringIO("A Madrid Move London\nB Barcelona Support A\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Barcelona\nC [dead]\n") 
    def test_solve_11(self):
        r = StringIO("A Madrid Move London\nB Barcelona Support C\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Barcelona\nC Madrid\n")
    def test_solve_12(self):
        r = StringIO("B Barcelona Support C\nA Madrid Move London\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Barcelona\nC Madrid\n")
    def test_solve_13(self):
        r = StringIO("B Barcelona Move Madrid\nA Madrid Move London\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\nC Barcelona\n")
    def test_solve_14(self):
        r = StringIO("A Austin Hold\nB Bangkok Move Austin\nC Cairo Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Austin\nC Cairo\n")
    def test_solve_15(self):
        r = StringIO("A NewYork Hold\nB Boston Move NewYork\nC Vancouver Move NewYork\nD Perth Support B\nE Dublin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Perth\nE Dublin\n")
    def test_solve_16(self):
        r = StringIO("A Austin Hold\n B Beijing Hold\nC Calgary Hold\nD Dublin Hold\nE Edinburgh Hold\nF Frankfurt Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Austin\nB Beijing\nC Calgary\nD Dublin\nE Edinburgh\nF Frankfurt\n")
    def test_solve_17(self):
        r = StringIO("A Austin Move SanAntonio\nB Dallas Support A\nC Houston Move Dallas\nD SanAntonio Support C\nE ElPaso Move Austin\nF Waco Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")
    def test_solve_18(self):
        r = StringIO("A Madrid Move Pluto\nB Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Pluto\nB Paris\n")     
    def test_solve_19(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Move Barcelona\nC Lisbon Move Barcelona\nD Vienna Move Barcelona\nE Berlin Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Barcelona\nE Berlin\n")

    ## testing supporting armies getting invaded
    def test_solve_20(self):
        r = StringIO("A Barcelona Support B\nB Austin Support C\nC Berlin Support A\nD Icky Move Austin ")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Austin\nC Berlin\nD [dead]\n") 
    def test_solve_21(self):
        r = StringIO("L Barcelona Support B\nB Austin Support C\nC Berlin Support L\nD Icky Move Austin\nE Wack Support D\nA Twickusmemen Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Twickusmemen\nB [dead]\nC Berlin\nD Austin\nE Wack\nL Barcelona\n") 

# ----
# main
# ----


if __name__ == "__main__": # pragma: no cover
    main()
