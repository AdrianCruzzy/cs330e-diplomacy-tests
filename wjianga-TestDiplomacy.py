from io import StringIO
from unittest import TestCase, main

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve

class TestDiplomacy(TestCase):
    # read in
    def test_read_1(self):
        s = "A Madrid Hold\n"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i, "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")

    def test_read_2(self):
        s = "B London Move Madrid\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "London")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "Madrid")

    def test_read_3(self):
        s = "C Tokyo Support D\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "Tokyo")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "D")

    # eval
    def test_eval_1(self):
        i = [["A", "Madrid", "Hold"]]
        o = list(diplomacy_eval(i))
        self.assertEqual(o, ["A Madrid"])

    def test_eval_2(self):
        i = [["A", "Madrid", "Hold"], 
             ["B", "Barcelona", "Move", "Madrid"],
             ["C", "London", "Support", "B"]]
        o = list(diplomacy_eval(i))
        self.assertEqual(o, ["A [dead]", "B Madrid", "C London"])

    def test_eval_3(self):
        i = [["A", "Madrid", "Hold"],
             ["B", "Barcelona", "Move", "Madrid"]]
        o = list(diplomacy_eval(i))
        self.assertEqual(o, ["A [dead]", "B [dead]"])

    # diplomacy_solve
    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

if __name__ == "__main__":
    main()